package main

import (
	"math"
)

type Kotak struct {
	Sisi float64
}

func (k Kotak) Volume() float64 {
	return math.Pow(k.Sisi, 3)
}

func (k Kotak) Luas() float64 {
	return math.Pow(k.Sisi, 2) * 6
}

func (k Kotak) Keliling() float64 {
	return k.Sisi * 12
}
