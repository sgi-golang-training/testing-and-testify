package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	kotak Kotak   = Kotak{4}
	v     float64 = 64
	l     float64 = 96
	k     float64 = 48
)

func TestHitungVolumeKotak(t *testing.T) {
	assert.Equal(t, kubus.Volume(), v, "perhitungan volume salah")
}

func TestHitungLuasKotak(t *testing.T) {
	assert.Equal(t, kubus.Luas(), l, "perhitungan luas salah")
}

func TestHitungKelilingKotak(t *testing.T) {
	assert.Equal(t, kubus.Keliling(), k, "perhitungan keliling salah")
}

func BenchmarkHitungLuasKotak(b *testing.B) {
	for i := 0; i < b.N; i++ {
		kotak.Luas()
	}
}
